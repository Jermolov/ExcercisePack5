package ee.bcs.valiit;

public class Skydiver extends Athlete{
    private String typeOfSkydiver;
    private String objectsHeJumpsFrom;

    public Skydiver(String firstName, String secondName, int age, String gender, int height, int weight, String typeOfSkydiver, String objectsHeJumpsFrom) {
        super(firstName, secondName, age, gender, height, weight);
        this.typeOfSkydiver = typeOfSkydiver;
        this.objectsHeJumpsFrom = objectsHeJumpsFrom;
    }

    public String getTypeOfSkydiver() {
        return typeOfSkydiver;
    }

    public void setTypeOfSkydiver(String typeOfSkydiver) {
        this.typeOfSkydiver = typeOfSkydiver;
    }

    public String getObjectsHeJumpsFrom() {
        return objectsHeJumpsFrom;
    }

    public void setObjectsHeJumpsFrom(String objectsHeJumpsFrom) {
        this.objectsHeJumpsFrom = objectsHeJumpsFrom;
    }

    @Override
    public String perform(String whatHeDoes) {
        return super.perform(whatHeDoes);
    }
}
