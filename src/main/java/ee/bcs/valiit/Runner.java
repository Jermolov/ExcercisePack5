package ee.bcs.valiit;

public class Runner extends Athlete {
    private int distanceRunner;
    private boolean freeTimeRunner;

    public Runner(String firstName, String secondName, int age, String gender, int height, int weight, int distanceRunner, boolean freeTimeRunner) {
        super(firstName, secondName, age, gender, height, weight);
        this.distanceRunner = distanceRunner;
        this.freeTimeRunner = freeTimeRunner;
    }

    public int getDistanceRunner() {
        return distanceRunner;
    }

    public void setDistanceRunner(int distanceRunner) {
        this.distanceRunner = distanceRunner;
    }

    public boolean isFreeTimeRunner() {
        return freeTimeRunner;
    }

    public void setFreeTimeRunner(boolean freeTimeRunner) {
        this.freeTimeRunner = freeTimeRunner;
    }

    @Override
    public String perform(String whatHeDoes) {
        return super.perform(whatHeDoes);
    }
}
