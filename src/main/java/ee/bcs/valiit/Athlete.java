package ee.bcs.valiit;

public class Athlete {
    private String firstName;
    private String secondName;
    private int age;
    private String gender;
    private int height;
    private int weight;

    public Athlete(String firstName, String secondName, int age, String gender, int height, int weight) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.age = age;
        this.gender = gender;
        this.height = height;
        this.weight = weight;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String sport(String sportType){
        switch (sportType){
            case "Athleticks":
                System.out.println("This athlete makes athleticks");
                break;
            case "Swimming":
                System.out.println("This athlete makes swimming");
                break;
                default:
                    System.out.println("Please say what athlete sport type is");
        }
        return sportType;
    }

    public String perform(String whatHeDoes){
        return whatHeDoes;
    }
}
